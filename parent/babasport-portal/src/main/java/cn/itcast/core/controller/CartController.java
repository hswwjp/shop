package cn.itcast.core.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.itcast.core.pojo.Cart;
import cn.itcast.core.pojo.Item;
import cn.itcast.core.service.CartService;
import cn.itcast.core.service.SessionService;
import cn.itcast.core.service.SkuService;
import cn.itcast.core.tools.SessionTool;

/**
 * 购物车控制器
 * 
 * @author Administrator
 *
 */
@Controller
public class CartController {

	@Autowired
	private SkuService skuService;

	@Autowired
	private CartService cartService;

	@Autowired
	private SessionService sessionService;

	// 显示购物车页面
	@RequestMapping(value = "/cart")
	public String showCart(Model model, HttpServletRequest request, HttpServletResponse response)
			throws JsonParseException, JsonMappingException, IOException {

		System.out.println("显示购物车");
		// 判断用户是否登录
		String username = sessionService.getUsernameForRedis(SessionTool.getSessionID(request, response));

		Cart cart = null; // 最终结果的购物车
		Cart cart1 = null; // cookie中的购物车
		Cart cart2 = null; // redis中的购物车

		// 从cookie取出购物车
		cart1 = this.getCartFormCookies(request);

		// 用户登录的情况
		if (username != null) {
			// 从redis中取出购物车
			cart2 = cartService.getCartFormRedis(username);
		}

		System.out.println("cart1:" + cart1);
		System.out.println("cart2:" + cart2);

		// 合并两处的购物车
		cart = this.mergeCart(cart1, cart2);

		// 如果是登录的情况
		if (username != null) {
			// 则清除cookie中的购物车
			this.delCartFormCookies(request, response);
			// 刷新redis里的购物车
			cartService.addCartToRedis(username, cart);
		}

		// 填充cart中的items中的skus信息
		cart = cartService.fillItemsSkus(cart);

		// 将此cart发送到页面
		model.addAttribute("cart", cart);
		return "cart";

	}

	// 添加购物车
	@RequestMapping(value = "/addCart")
	public String addCart(Model model, HttpServletRequest request, HttpServletResponse response, Long skuid,
			Integer amount) throws IOException {
		System.out.println("库存id" + skuid);
		System.out.println("购买数量" + amount);

		Cart cart = null;
		// 判断用户是否登录
		String username = sessionService.getUsernameForRedis(SessionTool.getSessionID(request, response));

		// 用户登录从redis中取出购物车
		if (username != null) {
			cart = cartService.getCartFormRedis(username);
		}
		// 用户没有登录
		else {
			// 从cookie中取出购物车对象
			cart = this.getCartFormCookies(request);
		}

		// 用户之前没有将商品加入过购物车
		if (cart == null) {
			cart = new Cart();
		}

		// 将相应的库存商品信息，添加到购物车对象中
		Item item = new Item();
		item.setAmount(amount);
		item.setSkuId(skuid);
		cart.addItem(item);

		// 用户登录
		if (username != null) {
			// 将购物车保存到redis
			cartService.addCartToRedis(username, cart);
		}
		// 用户没有登录
		else {
			System.out.println("mao" + cart.getItems().size());
			// 将新的购物车对象（也包括原来的数据） 覆盖到cookie中
			this.addCartToCookies(cart, response);
		}

		return "redirect:/cart";
	}

	// 结算 skuIds表示用户从购物车中选择的商品，本次课程先不做该功能
	@RequestMapping(value = "buyer/trueBuy")
	public String trueBuy(Long[] skuIds, Model model, HttpServletRequest request, HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {
		
		String username = sessionService.getUsernameForRedis(SessionTool.getSessionID(request, response));

		// 无货标识
		Boolean flag = true;

		// 判断redis里的购物车不能为空，也不能是空车子
		Cart cart = cartService.getCartFormRedis(username);
		if (cart != null && cart.getItems().size() > 0) {
			// 填充购物车 复合信息（具体库存相关信息）
			cart = cartService.fillItemsSkus(cart);
			// 判断库存一定够
			List<Item> items = cart.getItems();
			for (Item item : items) {
				// 购买数量大于库存数量
				if (item.getAmount() > Integer.parseInt(item.getSku().get("stock").toString())) {
					item.setIsHave(false);
					flag = false;
				}
			}
			// 至少有一款商品无货
			if (!flag) {
				model.addAttribute("cart", cart);
				return "/cart";
			}
		} else {
			// 回到空的购物车页面
			return "redirect:/cart";
		}
		// 最后有货进入订单页面
		return "order";

	}

	/**
	 * 将购物车从cookie中删除
	 * 
	 * @param response
	 */
	public void delCartFormCookies(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if ("cart".equals(cookie.getName())) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
				break;
			}
		}
	}

	/**
	 * 合并两处购物车对象
	 * 
	 * @param cart1
	 *            cookie中的购物车
	 * @param cart2
	 *            redis中的购物车
	 * @return
	 */
	public Cart mergeCart(Cart cart1, Cart cart2) {
		if (cart1 == null) {
			return cart2;
		} else if (cart2 == null) {
			return cart1;
		} else {
			// 则合并购物车
			List<Item> items = cart2.getItems();
			for (Item item : items) {
				cart1.addItem(item);
			}
			return cart1;
		}
	}

	/**
	 * 从cookie中取出购物车
	 * 
	 * @param request
	 * @return
	 */
	public Cart getCartFormCookies(HttpServletRequest request) {
		Cart cart = null;
		// 从cookie中取出购物车信息
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if ("cart".equals(cookie.getName())) {
				// 将购物车的json字符串转成购物车对象
				String value = cookie.getValue();
				ObjectMapper om = new ObjectMapper();
				try {
					cart = om.readValue(value, Cart.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
		}
		return cart;
	}

	/**
	 * 将新cart反存到cookies
	 * 
	 * @param cart
	 * @param response
	 * @throw)s JsonProcessingException
	 */
	public void addCartToCookies(Cart cart, HttpServletResponse response) throws JsonProcessingException {

		ObjectMapper om = new ObjectMapper();
		String cartJson = om.writeValueAsString(cart);

		Cookie cookie = new Cookie("cart", cartJson);

		cookie.setMaxAge(60 * 60 * 24 * 365 * 3);

		cookie.setPath("/");// 设置路径

		response.addCookie(cookie);
	}

}
