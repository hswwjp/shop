package cn.itcast.core.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cn.itcast.core.pojo.Order;
import cn.itcast.core.service.OrderService;
import cn.itcast.core.service.SessionService;
import cn.itcast.core.tools.SessionTool;

/**
 * 订单控制器
 * 
 * @author Administrator
 *
 */
@Controller
public class OrderController {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private OrderService orderService;

	// 提交订单
	@RequestMapping(value = "/buyer/submitOrder")
	public String submitOrder(Order order, Model model,
			HttpServletRequest request, HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		// 判断用户是否登录
		String username = sessionService.getUsernameForRedis(
				SessionTool.getSessionID(request, response));

		orderService.addOrderAndDetail(order, username);

		return "success";
	}

}

