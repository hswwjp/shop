package cn.itcast.core.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.Product;
import cn.itcast.core.pojo.SuperPojo;
import cn.itcast.core.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;

	// 显示单个商品页面
	@RequestMapping(value = "/product/detail")
	public String showSingleProduct(Model model, Long productId) {
		System.out.println("商品id:" + productId);
		SuperPojo superPojo = productService.findById(productId);

		Product product = (Product) superPojo.get("product");
		List skus = (List) superPojo.get("skus");

		System.out.println("商品名称：" + product.getName());
		System.out.println("库存数量：" + skus.size());

		// 去除颜色重复
		HashMap<Long, String> colors = new HashMap<Long, String>();

		for (Object object : skus) {
			SuperPojo sku = (SuperPojo) object;
			// 将颜色添加到hm集合中，利用hm集合来去除重复 key是颜色的id value是颜色名称
			colors.put((Long) sku.get("color_id"), (String) sku.get("name"));
		}

		// 反正万能实体对象要被传递，将非重复的颜色对象也通过superPojo顺便传递到页面
		superPojo.setProperty("colors", colors);

		// 将商品对象和该商品的库存对象的集合(万能pojo对象)传递给页面
		model.addAttribute("superPojo", superPojo);

		return "product";
	}

}
