package cn.itcast.core.controller;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.Brand;
import cn.itcast.core.pojo.Product;
import cn.itcast.core.service.ProductService;
import cn.itcast.core.tools.Encoding;
import cn.itcast.core.tools.PageHelper.Page;

/**
 * 商品管理控制器
 * 
 * @author jie
 *
 */
@Controller
public class ProductController {
	@Autowired
	private ProductService productService;

	// 通用的商品页面跳转
	@RequestMapping(value = "console/product/{pageName}.do")
	public String consoleProductShow(@PathVariable(value = "pageName") String pageName) {
		return "product/" + pageName;
	}

	// 显示商品列表（具体映射）
	@RequestMapping(value = "console/product/list.do")
	public String consoleProductShowList(Model model, String name, Long brandId, Integer isShow, Integer pageNum,
			Integer pageSize) {

		// 设置查询条件
		// 只演示查name，其它条件省略掉了，如果是get方式，注意处理乱码
		Product product = new Product();
		if (name != null) {
			product.setName(Encoding.encodeGetRequest(name));
		}
		if (brandId != null) {
			product.setBrandId(brandId);
		}
		product.setIsShow(isShow);

		Page<Product> pageProduct = productService.findByExample(product, pageNum, pageSize);

		List<Brand> brands = productService.findBrands();

		model.addAttribute("brands", brands);

		// 将查询出来的品牌集合传递给页面
		model.addAttribute("pageProduct", pageProduct);
		// 设置查询数据回显之将查询数据传回给页面
		model.addAttribute("product", product);
		// model.addAttribute("brandId", brandId);

		// 分页页码的显示
		int begin = 1;
		int end = 0;
		if (pageProduct.getPages() >= 10) {
			end = 10;
		} else {
			end = pageProduct.getPages();
		}
		if (pageNum != null) {
			if (pageProduct.getPages() <= 10) {
				begin = 1;
				end = pageProduct.getPages();
			} else {
				begin = pageNum - 5;
				end = pageNum + 4;
				if (begin <= 1) {
					begin = 1;
					end = 10;
				}
				if (end >= pageProduct.getPages()) {
					begin = pageProduct.getPages() - 9;
					end = pageProduct.getPages();
				}
			}
		}
		model.addAttribute("begin", begin);
		model.addAttribute("end", end);

		return "/product/list";
	}

	// 显示商品添加（具体映射）
	@RequestMapping(value = "console/product/showAdd.do")
	public String consoleBrandShowAdd(Model model) {
		// 加载所有可用颜色
		model.addAttribute("colors", productService.findEnableColors());
		return "/product/add";
	}

	// 添加商品
	@RequestMapping(value = "console/product/doAdd.do")
	public String consoleProductDoAdd(Model model, Product product) {
		productService.add(product);
		return "redirect:/console/product/list.do";
	}

	// 商品上架下架 isShow=1表示上架，=0表示下架
	@RequestMapping(value = "console/product/isShow.do")
	public String consoleProductDoIsShow(String ids, Integer isShow) throws SolrServerException, IOException {
		System.out.println(ids);
		System.out.println(isShow);

		// 设置修改结果，（是否上架）
		Product product = new Product();
		product.setIsShow(isShow);

		productService.update(product, ids);

		return "redirect:/console/product/list.do";
	}

}
