package cn.itcast.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.itcast.core.pojo.Sku;
import cn.itcast.core.service.SkuService;

/**
 * 库存管理控制
 * 
 * @author jie
 *
 */
@Controller
public class SkuController {

	@Autowired
	private SkuService skuService;

	@RequestMapping(value = "console/sku/{pageNum}.do")
	public String consoleSkuShow(@PathVariable(value = "pageNum") String pageNum) {
		System.out.println("sku");
		return "sku/" + pageNum;
	}

	// 显示某商品的库存列表（具体映射）
	@RequestMapping(value = "console/sku/list.do")
	public String consoleSkuShowList(Model model, Long productId) {
		System.out.println(productId);
		List<Sku> skus = skuService.findByProductId(productId);
		System.out.println("库存数量：" + skus.size());
		model.addAttribute("skus", skus);

		return "/sku/list";
	}

	// 修改商品库存信息
	@RequestMapping(value = "console/sku/update.do")
	@ResponseBody
	public String consoleSkuDoUpdate(Sku sku) {
		int update = skuService.update(sku);
		return update + "";
	}

}
