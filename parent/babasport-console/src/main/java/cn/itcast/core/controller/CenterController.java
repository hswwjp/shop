package cn.itcast.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 后台管理 控制中心
 * 
 * @author Administrator
 *
 */
@Controller

public class CenterController {

	// 通用的action
	@RequestMapping(value = "/console/{pageName}.do")
	public String consoleShow(Model model, @PathVariable(value = "pageName") String pageName) {
		System.out.println("通用：" + pageName);

		return pageName;
	}

	// 通用的frame
	@RequestMapping(value = "/console/frame/{pageName}.do")
	public String consoleFrameShow(Model model, @PathVariable(value = "pageName") String pageName) {
		System.out.println("通用frame：" + pageName);

		return "frame/" + pageName;
	}

}
