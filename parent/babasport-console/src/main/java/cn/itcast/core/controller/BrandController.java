package cn.itcast.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.Brand;
import cn.itcast.core.service.BrandService;
import cn.itcast.core.tools.Encoding;
import cn.itcast.core.tools.PageHelper.Page;

/**
 * 品牌管理控制器
 * 
 * @author jie
 *
 */
@Controller
public class BrandController {

	@Autowired
	private BrandService brandService;

	// 通用的品牌页面跳转
	@RequestMapping(value = "console/brand/{pageName}.do")
	public String consoleBrandShow(@PathVariable(value = "pageName") String pageName) {
		return "brand/" + pageName;
	}

	// 品牌
	@RequestMapping(value = "console/brand/list.do")
	public String consoleBrandShow(Model model, String name, Integer isDisplay, Integer pageNum, Integer pageSize) {

		// 品牌查询

		// 设置查询条件
		Brand brand = new Brand();
		brand.setName(Encoding.encodeGetRequest(name));
		brand.setIsDisplay(isDisplay);

		// 开始分页查询
		Page<Brand> pageBrand = brandService.findByExample(brand, pageNum, pageSize);

		// 将查询出来的品牌集合传递给页面
		model.addAttribute("pageBrand", pageBrand);

		// 设置查询数据回显之将查询数据传回给页面
		model.addAttribute("name", Encoding.encodeGetRequest(name));
		model.addAttribute("isDisplay", isDisplay);

		// 分页页码的显示
		int begin = 1;
		int end = 0;
		if (pageBrand.getPages() >= 10) {
			end = 10;
		} else {
			end = pageBrand.getPages();
		}
		if (pageNum != null) {
			if (pageBrand.getPages() <= 10) {
				begin = 1;
				end = pageBrand.getPages();
			} else {
				begin = pageNum - 5;
				end = pageNum + 4;
				if (begin <= 1) {
					begin = 1;
					end = 10;
				}
				if (end >= pageBrand.getPages()) {
					begin = pageBrand.getPages() - 9;
					end = pageBrand.getPages();
				}
			}
		}
		model.addAttribute("begin", begin);
		model.addAttribute("end", end);

		return "/brand/list";
	}

	// 显示修改页面（具体映射）
	@RequestMapping(value = "console/brand/showEdit.do")
	public String consoleBrandShowEdit(Long brandId, Model model) {
		// 获得要修改品牌的id
		System.out.println(brandId);
		// 设置修改的数据回显
		Brand brand = brandService.findById(brandId);
		model.addAttribute("brand", brand);
		return "/brand/edit";
	}

	// 执行品牌修改
	@RequestMapping(value = "console/brand/doEdit.do")
	public String consoleBrandDoEdit(Brand brand) {

		// 获得要修改的对象内容
		System.out.println(brand);
		// 根据id来执行修改
		brandService.updateById(brand);
		// 重定向到显示品牌列表功能页面
		return "redirect:/console/brand/list.do";
	}

	// 品牌删除
	@RequestMapping(value = "console/brand/doDelete.do")
	public String consoleBrandDoDelete(String ids, String name, Integer isDisplay, Integer pageNum) {
		brandService.deleteByIds(ids);
		if (isDisplay == null) {
			return "redirect:list.do?name=" + name + "&pageNum=" + pageNum;
		}
		return "redirect:/console/brand/list.do?name=" + name + "&isDisplay=" + isDisplay + "&pageNum=" + pageNum;
	}

}