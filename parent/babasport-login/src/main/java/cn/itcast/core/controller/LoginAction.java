package cn.itcast.core.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.itcast.core.pojo.Buyer;
import cn.itcast.core.service.BuyerService;
import cn.itcast.core.service.SessionService;
import cn.itcast.core.tools.Encoding;
import cn.itcast.core.tools.Encryption;
import cn.itcast.core.tools.SessionTool;

/**
 * 登录控制器
 * 
 * @author Administrator
 *
 */
@Controller
public class LoginAction {

	@Autowired
	private BuyerService buyerService;

	@Autowired
	private SessionService sessionService;

	// 显示登录页面
	@RequestMapping(value = "/login.aspx", method = RequestMethod.GET)
	public String showLogin() {
		System.out.println("显示登录页面");
		return "login";
	}

	// 执行登录功能
	@RequestMapping(value = "/login.aspx", method = RequestMethod.POST)
	public String doLogin(Model model, String username, String password, String returnUrl, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println(username);
		System.out.println(password);
		System.out.println(returnUrl);

		// 如果有验证码，还需要判断验证码

		// 用户名不为空时
		if (username != null) {
			// 密码不为空时
			if (password != null) {
				// 开始查数据库检测用户名和密码是否正确
				Buyer buyer = buyerService.findByUsername(username);

				System.out.println(buyer);

				// 当用户名存在时
				if (buyer != null) {
					// 开始判断密码

					// 因为数据库中的密码已经是不可逆加密过后的密文，所以比较密码时先对用户传入的密码进行加密
					if (buyer.getPassword().equals(Encryption.encrypt(password))) {
						// 当密码也正确时，进行session相关操作
						System.out.println("用户名和密码都正确");

						// 将用户名保存到自定义session中(redis)
						sessionService.addUsernameToRedis(SessionTool.getSessionID(request, response), username);

						// 如果用户直接打开登录页面，则登录后返回首页
						if (returnUrl == null || returnUrl.length() < 1) {
							returnUrl = "http://localhost:8082/";
						}
						return "redirect:" + (Encoding.encodeGetRequest(returnUrl));

					} else {
						model.addAttribute("error", "密码不正确");
					}

				} else {
					model.addAttribute("error", "该用户不存在");
				}

			} else {
				model.addAttribute("error", "密码不能为空");
			}
		} else {
			model.addAttribute("error", "用户名不能为空");
		}

		return "login";
	}

	// 判断用户是否登录
	@RequestMapping(value = "/isLogin.aspx")
	@ResponseBody
	public MappingJacksonValue isLogin(String callback, HttpServletRequest request, HttpServletResponse response) {

		System.out.println("检查用户是否登录");
		System.out.println(callback);
		
		// 根据maosessionid去redis上查找相应的用户名
		String username = sessionService.getUsernameForRedis(SessionTool.getSessionID(request, response));

		System.out.println("sessionUser:" + username);
		
		MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(username);
		mappingJacksonValue.setJsonpFunction(callback);

		return mappingJacksonValue;
	}

}
