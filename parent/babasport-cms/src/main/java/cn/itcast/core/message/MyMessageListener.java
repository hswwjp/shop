package cn.itcast.core.message;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;

import cn.itcast.core.pojo.Color;
import cn.itcast.core.pojo.SuperPojo;
import cn.itcast.core.service.ProductService;
import cn.itcast.core.service.StaticPageService;
import freemarker.template.TemplateException;

public class MyMessageListener implements MessageListener {

	@Autowired
	private StaticPageService staticPageService;

	@Autowired
	private ProductService productService;

	@Override
	public void onMessage(Message message) {
		TextMessage amessage = (TextMessage) message;
		try {
			String ids = amessage.getText();
			System.out.println("cms ids:" + ids);

			// 根据ids去查询数据库，并构建数据模型，完成静态页面的生成。。。。
			String[] split = ids.split(",");
			for (String id : split) {

				SuperPojo superPojo = productService.findById(Long.valueOf(id));

				List skus = (List) superPojo.get("skus");

				// 去除颜色重复
				Set<Color> colors = new HashSet<Color>();

				for (Object object : skus) {
					SuperPojo sku = (SuperPojo) object;
					// 将颜色添加到hm集合中，利用hm集合来去除重复 key是颜色的id value是颜色名称
					Color color = new Color();
					color.setId((Long) sku.get("color_id"));
					color.setName((String) sku.get("name"));
					colors.add(color);
				}

				// 反正万能实体对象要被传递，将非重复的颜色对象也通过superPojo顺便传递到页面
				superPojo.setProperty("colors", colors);
				
				// 将商品对象和该商品的库存对象的集合(万能pojo对象)当成freemark数据模型
				HashMap<String, Object> rootMap = new HashMap<>();
				rootMap.put("superPojo", superPojo);

				staticPageService.staticProductPage(rootMap, id);
			}

		} catch (JMSException | IOException | TemplateException e) {
			e.printStackTrace();
		}
	}

}
