package cn.itcast.core.dao;

import java.util.List;

import com.github.abel533.mapper.Mapper;

import cn.itcast.core.pojo.Product;

public interface ProductDAO extends Mapper<Product> {

}
