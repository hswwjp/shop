package cn.itcast.core.dao;

import com.github.abel533.mapper.Mapper;

import cn.itcast.core.pojo.Detail;

/**
 * 订单详情DAO
 * 
 * @author Administrator
 *
 */
public interface DetailDAO extends Mapper<Detail> {

}
