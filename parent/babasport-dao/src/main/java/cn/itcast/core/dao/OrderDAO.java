package cn.itcast.core.dao;

import com.github.abel533.mapper.Mapper;

import cn.itcast.core.pojo.Order;

/**
 * 订单DAO
 * @author Administrator
 *
 */
public interface OrderDAO extends Mapper<Order>{

}

