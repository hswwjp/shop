package cn.itcast.core.dao;

import cn.itcast.core.pojo.TestTb;

public interface TestTbDAO {
	
	/**
	 * 添加测试实体类
	 * @param testTb
	 */
	public void add(TestTb testTb);
}
