package cn.itcast.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.abel533.mapper.Mapper;

import cn.itcast.core.pojo.Brand;

public interface BrandDAO extends Mapper<Brand> {

	/**
	 * 根据案例条件查询品牌列表
	 * 
	 * @param brand
	 * @return
	 */
	List<Brand> findByExample(Brand brand);

	/**
	 * 根据id查询 单个品牌对象信息
	 * 
	 * @param id
	 * @return
	 */
	public Brand findById(Long id);

	/**
	 * 修改单个品牌对象信息
	 * 
	 * @param brand
	 */
	public void updateById(Brand brand);

	/**
	 * 根据多个id删除品牌对象
	 * 
	 * @param ids
	 */
	public void deleteByIds(String ids);

}
