package cn.itcast.core.tools;

import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * session工具类 负责分配maosid，以及存取cookies
 * 
 * 1、分配maosid
 * 
 * 2、maosid要写入到cookies中。 response
 * 
 * 3、从cookies中取出maosid。request
 * 
 * @author Administrator
 *
 */
public class SessionTool {

	/**
	 * 首先检查请求中有没有maosid，如果没有就分配一个maosid，并返回给浏览器cookies中
	 * 
	 * 获得自定义sessionID，并在初次访问时，分配maosessionid，并将maosessionid写入到浏览器的cookie中
	 * 
	 * @return
	 */
	public static String getSessionID(HttpServletRequest request, HttpServletResponse response) {

		// 检查请求中有没有maosid
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("maosessionid")) {
					System.out.println("maosessionid:" + cookie.getValue());
					return cookie.getValue();
				}
			}
		}

		// 分配新的maosid
		String maosessionid = UUID.randomUUID().toString().replaceAll("-", "");

		System.out.println(maosessionid);

		// 并将maosid存入到cookies中
		Cookie cookie = new Cookie("maosessionid", maosessionid);

		// 设置cookie存活时间
		cookie.setMaxAge(-1);

		// 设置二级跨域，由于本次课程中都是localhost，所有无需设置，但是项目正式上限后需要设置该项
		// cookie.setDomain(".babasport.com");

		// 设置路径 如果不设置，端口后面的目录名称（xxx:8080/login）会对cookie存储照成影响
		cookie.setPath("/");

		// 把cookie写回浏览器客户端
		response.addCookie(cookie);

		return maosessionid;

	}

	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
	}

}
