package cn.itcast.core.pojo;

import java.io.Serializable;

/**
 * 购买项
 * 
 * @author Administrator
 *
 */
public class Item implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 库存id（单独提出来方便操作）
	private Long skuId;

	// 复合型的sku（SuperPojo）
	private SuperPojo sku;

	// 购买数量 amount
	private Integer amount;
	
	// 有货无货标识
	private Boolean isHave = true;

	public Boolean getIsHave() {
		return isHave;
	}

	public void setIsHave(Boolean isHave) {
		this.isHave = isHave;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public SuperPojo getSku() {
		return sku;
	}

	public void setSku(SuperPojo sku) {
		this.sku = sku;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}
}
