package cn.itcast.core.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.itcast.core.dao.SkuDAO;
import cn.itcast.core.pojo.Cart;
import cn.itcast.core.pojo.Item;
import cn.itcast.core.pojo.SuperPojo;
import redis.clients.jedis.Jedis;

/**
 * 购物车服务实现类
 * 
 * @author Administrator
 *
 */
@Service("cartService")
public class CartServiceImpl implements CartService {

	@Autowired
	private SkuDAO skuDAO;

	@Autowired
	private Jedis jedis;

	@Override
	public Cart fillItemsSkus(Cart cart) {

		if (cart == null) {
			return null;
		}

		List<Item> items = cart.getItems();
		for (Item item : items) {
			Long skuId = item.getSkuId();
			// 根据skuid去数据库查询出sku对象的复合信息（颜色名称、商品名称。。。。）
			SuperPojo sku = skuDAO.findSKuAndColorAndProductBySkuId(skuId);
			item.setSku(sku);
		}
		return cart;
	}

	@Override
	public Cart getCartFormRedis(String username) throws JsonParseException, JsonMappingException, IOException {
		
		// 从redis中取出cart的json字符串
		String str = jedis.get("cart:" + username);

		if (str == null) {
			return null;
		}

		// 将cart的json字符串转成cart对象
		ObjectMapper om = new ObjectMapper();
		Cart cart = om.readValue(str, Cart.class);
		
		return cart;
	}

	@Override
	public void addCartToRedis(String username, Cart cart) throws JsonProcessingException {
		// key --> cart:'username'
		// value --> json(cart)

		// 将cart对象变成json字符串
		ObjectMapper om = new ObjectMapper();
		String cartJson = om.writeValueAsString(cart);

		// 将cart对象的json字符串放入到redis中
		jedis.set("cart:" + username, cartJson);
	}

}
