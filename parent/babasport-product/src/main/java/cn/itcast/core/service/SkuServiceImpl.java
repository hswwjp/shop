package cn.itcast.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.abel533.entity.Example;

import cn.itcast.core.dao.SkuDAO;
import cn.itcast.core.pojo.Sku;
import cn.itcast.core.pojo.SuperPojo;

@Service("skuService")
public class SkuServiceImpl implements SkuService {

	@Autowired
	private SkuDAO skuDAO;

	@Override
	public List<Sku> findByProductId(Long productId) {

		Example example = new Example(Sku.class);
		example.createCriteria().andEqualTo("productId", productId);
		List<Sku> skus = skuDAO.selectByExample(example);
		return skus;
	}

	@Override
	public int update(Sku sku) {
		// updateByPrimaryKeySelective 表示选择性的修改，只会修改对象中有值的属性到数据库中
		int i = skuDAO.updateByPrimaryKeySelective(sku);
		return i;
	}

	@Override
	public List<SuperPojo> findSKuAndColorByProductId(Long productId) {
		return skuDAO.findSKuAndColorByProductId(productId);
	}

	@Override
	public SuperPojo findSKuAndColorAndProductBySkuId(Long skuId) {
		return skuDAO.findSKuAndColorAndProductBySkuId(skuId);
	}

}
