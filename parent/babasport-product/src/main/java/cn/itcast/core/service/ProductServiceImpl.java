package cn.itcast.core.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.github.abel533.entity.Example;
import com.github.abel533.entity.Example.Criteria;

import cn.itcast.core.dao.BrandDAO;
import cn.itcast.core.dao.ColorDAO;
import cn.itcast.core.dao.ProductDAO;
import cn.itcast.core.dao.SkuDAO;
import cn.itcast.core.pojo.Brand;
import cn.itcast.core.pojo.Color;
import cn.itcast.core.pojo.Product;
import cn.itcast.core.pojo.Sku;
import cn.itcast.core.pojo.SuperPojo;
import cn.itcast.core.tools.PageHelper;
import cn.itcast.core.tools.PageHelper.Page;
import redis.clients.jedis.Jedis;

@Service("productService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	@Autowired
	private BrandDAO brandDAO;

	@Autowired
	private ColorDAO colorDAO;

	@Autowired
	private SkuDAO skuDAO;

	@Autowired
	private Jedis jedis;

	@Autowired
	private SolrServer solrServer;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Override
	public Page<Product> findByExample(Product product, Integer pageNum, Integer pageSize) {
		Example example = new Example(Product.class);
		example.setOrderByClause("createTime desc");
		if (product.getName() == null || " ".equals(product.getName())) {
			product.setName("");
		}
		PageHelper.startPage(pageNum, pageSize);
		Criteria criteria = example.createCriteria();

		criteria.andLike("name", "%" + product.getName() + "%");
		if (product.getBrandId() != null) {
			criteria.andEqualTo("brandId", product.getBrandId());
		}
		if (product.getIsShow() != null) {
			criteria.andEqualTo("isShow", product.getIsShow());
		}

		List<Product> selectByExample = productDAO.selectByExample(example);

		Page page = PageHelper.endPage();

		return page;
	}

	@Override
	public List<Brand> findBrands() {
		List<Brand> list = brandDAO.findByExample(null);
		return list;
	}

	@Override
	public List<Color> findEnableColors() {

		Example example = new Example(Color.class);
		example.createCriteria().andNotEqualTo("parentId", 0 + "");

		List<Color> colors = colorDAO.selectByExample(example);
		return colors;
	}

	@Override
	public void add(Product product) {

		// 设置默认值
		if (product.getIsShow() == null) {
			product.setIsShow(0);
		}
		if (product.getCreateTime() == null) {
			product.setCreateTime(new Date());
		}

		// 添加商品
		Long pno = jedis.incr("pno");
		product.setId(pno);

		// 先添加商品到数据库中
		productDAO.insert(product);
		System.out.println("获得回显id" + product.getId());

		// 将商品信息添加到库存表中
		// 遍历不同的颜色和尺码
		// 每一个不同颜色，或者不同尺码，都应该插入库存表中，成为一条数据
		String[] colors = product.getColors().split(",");
		String[] sizes = product.getSizes().split(",");

		for (String color : colors) {
			for (String size : sizes) {
				Sku sku = new Sku();
				sku.setProductId(product.getId());
				sku.setColorId(Long.parseLong(color));
				sku.setSize(size);
				sku.setMarketPrice(1000.00f);
				sku.setPrice(800.00f);
				sku.setDeliveFee(20f);
				sku.setStock(0);
				sku.setUpperLimit(100);
				sku.setCreateTime(new Date());

				skuDAO.insert(sku);
			}
		}
	}

	@Override
	public void update(Product product, final String ids) throws SolrServerException, IOException {
		Example example = new Example(Product.class);

		// 将ids的字符串转成list集合
		List arrayList = new ArrayList();
		String[] split = ids.split(",");
		for (String string : split) {
			arrayList.add(string);
		}

		// 设置批量修改的id条件
		example.createCriteria().andIn("id", arrayList);

		// 进行批量，选择性的非空属性修改
		productDAO.updateByExampleSelective(product, example);

		// 如果是商品上架，将商品信息添加到solr服务器中
		// 需要保存的信息有：商品id、商品名称、图片地址、售价、品牌id、上架时间（可选）
		if (product.getIsShow() == 1) {

			// 采用消息服务模式
			// 商品上架将商品信息添加到solr服务器中（发送消息（ids）到ActiveMQ中）
			jmsTemplate.send("productIds", new MessageCreator() {

				@Override
				public Message createMessage(Session session) throws JMSException {
					// 使用session创建文本消息
					return session.createTextMessage(ids);
				}
			});

			// 查询ids中的所有商品
			List<Product> products = productDAO.selectByExample(example);
			// 遍历查询出来的商品集合
			for (Product product2 : products) {

				// 将商品的各个信息，添加到文档对象中
				SolrInputDocument doc = new SolrInputDocument();
				doc.addField("id", product2.getId());
				doc.addField("name_ik", product2.getName());
				doc.addField("url", product2.getImgUrl().split(",")[0]);
				doc.addField("brandId", product2.getBrandId());

				// 查询出某商品库存中的最低价格
				// SELECT price from bbs_sku WHERE bbs_sku.product_id = 449
				// ORDER BY price ASC LIMIT 1

				Example example2 = new Example(Sku.class);
				// 某商品的库存
				example2.createCriteria().andEqualTo("productId", product2.getId());
				example2.setOrderByClause("price asc");// 价格升序
				// 开始分页 limit
				PageHelper.startPage(1, 1);
				List<Sku> skus = skuDAO.selectByExample(example2);
				// 结束分页
				PageHelper.endPage();

				doc.addField("price", skus.get(0).getPrice());

				// 将文档对象添加到solr服务器中
				solrServer.add(doc);

				// 提交
				solrServer.commit();
			}
		}
	}
	
	@Override
	public SuperPojo findById(Long id) {
		// 先查询出单个商品对象
		Product product = productDAO.selectByPrimaryKey(id);
		
		// 再根据商品id查询出该商品的库存对象集合
		// Sku sku = new Sku();
		// sku.setProductId(id);
		// List<Sku> skus = skuDAO.select(sku);
		
		// 此时的Sku因为里面与颜色信息，所以可以用万能POJO对象来装载
		List<SuperPojo> skus = skuDAO.findSKuAndColorByProductId(id);
		
		// 将单个商品对象和其对应的库存对象集合封装到万能pojo对象中
		SuperPojo superPojo = new SuperPojo();
		superPojo.setProperty("product", product);
		superPojo.setProperty("skus", skus);
		
		return superPojo;
	}

}
