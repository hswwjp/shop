package cn.itcast.core.service;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;

import cn.itcast.core.pojo.Brand;
import cn.itcast.core.pojo.Color;
import cn.itcast.core.pojo.Product;
import cn.itcast.core.pojo.SuperPojo;
import cn.itcast.core.tools.PageHelper.Page;

public interface ProductService {

	/**
	 * 根据条件查询商品信息
	 * 
	 * @param product
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	Page<Product> findByExample(Product product, Integer pageNum, Integer pageSize);

	/**
	 * 查询全部品牌
	 * 
	 * @return
	 */
	public List<Brand> findBrands();

	/**
	 * 查询所有可用颜色（颜色的父id不为0）
	 * 
	 * @return
	 */
	public List<Color> findEnableColors();

	/**
	 * 添加商品
	 * 
	 * @param product
	 */
	public void add(Product product);

	/**
	 * 批量修改商品统一信息
	 * 
	 * @param product
	 * @throws IOException
	 * @throws SolrServerException
	 */
	void update(Product product, String ids) throws SolrServerException, IOException;

	/**
	 * 根据商品id查询单个商品信息
	 * 
	 * @param id
	 * @return
	 */
	SuperPojo findById(Long productId);

}
