package cn.itcast.core.service;

import java.util.List;

import cn.itcast.core.pojo.Sku;
import cn.itcast.core.pojo.SuperPojo;

public interface SkuService {

	/**
	 * 根据商品id查找该商品的库存信息
	 * 
	 * @param producId
	 * @return
	 */
	public List<Sku> findByProductId(Long productId);

	/**
	 * 修改库存
	 * 
	 * @param sku
	 * @return
	 */
	public int update(Sku sku);

	/**
	 * 根据商品id查询某商品的库存，并且将颜色名称，通过对颜色表连接查询的方式也带出来
	 * 
	 * @param productId
	 * @return
	 */
	public List<SuperPojo> findSKuAndColorByProductId(Long productId);

	
	/**
	 * 根据库存id查询某商品的库存，并且将颜色名称，商品名称等通过对颜色表、商品表连接查询的方式也带出来
	 * 
	 * @param skuId
	 * @return
	 */
	public SuperPojo findSKuAndColorAndProductBySkuId(Long skuId);
	
}