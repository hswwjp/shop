package cn.itcast.core.service;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;

import cn.itcast.core.pojo.SuperPojo;
import cn.itcast.core.tools.PageHelper.Page;

public interface SolrService {

	/**
	 * 根据关键字搜索商品
	 * 
	 * @param sort
	 * @param pageSize
	 * @param pageNum
	 * @param pb
	 * @param pa
	 * @param brandId
	 * 
	 * @param keyWord
	 * @return
	 * @throws SolrServerException
	 */
	Page<SuperPojo> findProductByKeyWord(String keyword, String sort, Integer pageNum, Integer pageSize, Long brandId,
			Float pa, Float pb) throws SolrServerException;

	/**
	 * 添加商品到solr服务器中
	 * 
	 * @param ids
	 * @throws SolrServerException
	 * @throws IOException
	 */
	void addProduct(String ids) throws SolrServerException, IOException ;

}
