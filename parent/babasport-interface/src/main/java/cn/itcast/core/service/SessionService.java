package cn.itcast.core.service;

/**
 * session服务接口
 * 
 * 1、将maosid配合登录的用户名存入到redis中。
 * 
 * 2、根据maosid去redis中找出对应的用户名。
 * 
 * @author Administrator
 *
 */
public interface SessionService {

	/**
	 * 保存用户名到redis中
	 * 
	 * @param key
	 *            uuid
	 * @param value
	 */
	public void addUsernameToRedis(String key, String value);

	/**
	 * 从redis中取出用户名
	 * 
	 * @param key
	 * @return
	 */
	public String getUsernameForRedis(String key);

}
