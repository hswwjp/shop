package cn.itcast.core.service;

import java.util.List;

import cn.itcast.core.pojo.Brand;
import cn.itcast.core.tools.PageHelper.Page;

public interface BrandService {

	/**
	 * 根据案例条件查询品牌列表
	 * 
	 * @param brand
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public Page findByExample(Brand brand, Integer pageNum, Integer pageSize);

	/**
	 * 根据id查询
	 * 
	 * @return
	 */
	public Brand findById(Long id);

	/**
	 * 根据id修改品牌
	 * 
	 * @return
	 */
	public void updateById(Brand brand);

	/**
	 * 根据多个id删除品牌对象
	 * 
	 * @param ids
	 */
	public void deleteByIds(String ids);

	/**
	 * 从redis中查询所有品牌
	 * 
	 * @return
	 */
	public List<Brand> findAllFromRedis();

}
