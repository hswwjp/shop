package cn.itcast.core.message;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.abel533.entity.Example;

import cn.itcast.core.dao.ProductDAO;
import cn.itcast.core.dao.SkuDAO;
import cn.itcast.core.pojo.Product;
import cn.itcast.core.pojo.Sku;
import cn.itcast.core.tools.PageHelper;
import cn.itcast.core.tools.PageHelper.Page;

public class MyMessageListener implements MessageListener {
	
	@Autowired
	private ProductDAO productDAO;

	@Autowired
	private SkuDAO skuDAO;

	@Autowired
	private HttpSolrServer solrServer;

	@Override
	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			String ids = textMessage.getText();
			System.out.println("消费方接收到的消息：" + ids);
			// 添加商品信息到solr服务器
			String[] split = ids.split(",");
			// 将商品的变异信息添加到solr索引库中。
			for (String id : split) {

				// 根据商品的id查询出单个商品的信息
				Product product2 = productDAO.selectByPrimaryKey(Long.parseLong(id));

				SolrInputDocument document = new SolrInputDocument();

				// id
				document.addField("id", id);

				// name
				document.addField("name_ik", product2.getName());

				// brandId
				document.addField("brandId", product2.getBrandId());

				// 商品的首张图片
				document.addField("url", product2.getImgUrl().split(",")[0]);

				// 该商品旗下的所有库存的最低价格
				// select * from bbs_sku as s where s.product_id = 438 order BY
				// price ASC LIMIT 0,1;
				Example example2 = new Example(Sku.class);
				example2.createCriteria().andEqualTo("productId", product2.getId());
				example2.setOrderByClause("price asc");
				PageHelper.startPage(1, 1);
				skuDAO.selectByExample(example2);
				Page<Sku> endPage = PageHelper.endPage();
				Float price = endPage.getResult().get(0).getPrice();
				document.addField("price", price);

				solrServer.add(document);
			}
			solrServer.commit();
		} catch (JMSException | SolrServerException | IOException e) {
			e.printStackTrace();
		}
		System.out.println(textMessage);
	}

}
